# Microservicio Programaciones

## Clonar

```sh
$ git clone https://bitbucket.org/amarischile/ms-proplan-programaciones.git
$ cd ms-proplan-programaciones
$ cp .env .env.local
$ npm install
$ npm test
$ npm run start:local
```
> Debes configurar las variables de entorno para tu desarrollo local en .env.local


## Docker

```sh
$ docker build -t ms-proplan-programaciones .

$ docker run -it \
-e APP_PORT=1003 \
-e APP_HOST=0.0.0.0 \
-e APP_PREFIX=/ \
-e APP_HEALTH_PATH=/health \
-e MYSQL_CONNECTION_STRING="mysql://user:pass@server/dbname?debug=true&charset=UTF8_GENERAL_CI" \
-p 1003:1003 --name ms-proplan-programaciones ms-proplan-programaciones
```

## Verificar

Verificar que el servicio este arriba

```sh
$ curl -v http://localhost:1003/health
```