import {parse} from "dotenv";

const moment = require('moment');

const mapToProgramaciones = (programciones) => {
    return programciones.map(programcion => {
        return mapToProgramacion(programcion);
    });
};

const mapToProgramacion = (programcion) => {
    if(programcion != null){
        return {
            id: programcion.id,
            actividad:{
                id: programcion.actividad_id,
                codigo: programcion.actividad_codigo,
                descripcion: programcion.actividad_descripcion,
            },
            especialidad:{
                id: programcion.especialidad_id,
                codigo: programcion.especialidad_codigo,
                descripcion: programcion.especialidad_descripcion,
            },   
            centro_costo:{
                id: programcion.centro_costo_id,
                codigo: programcion.centro_costo_codigo,
                descripcion: programcion.centro_costo_descripcion,
            },                       
            horas:{
                asignadas: parseFloat(programcion.horas_asignadas),
                rendimiento: parseFloat(programcion.horas_rendimiento),
                legales_descuento: parseFloat(programcion.horas_legales_descuento),
            },
            aprobado: programcion.aprobado,
            fecha_registro: moment(programcion.fecha_creacion_registro).format('DD-MM-YYYY HH:m:ss'),
        };
    }
    return null;
};

const mapPorcentajes = (arrayHoras) => {
    if (arrayHoras!= null) {
        const res = [];
        arrayHoras.forEach(r => {
            res.push({
                id: r.id,
                horas_programadas: parseFloat(r.horas_programadas),
                horas_semanales: parseFloat(r.horas_semanales)
            })
        });
        return res;
    }
    return null;
}

export {
    mapToProgramaciones,
    mapToProgramacion,
    mapPorcentajes,
};