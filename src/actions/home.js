import { 
    upsert, 
    obtenerPorContrato as rObtenerPorContrato,
    actualizar as rActualizar,
    obtenerPorId as rObtenerPorId,
    eliminar as rEliminar,
    cambiarEstadoAprobacion as rCambiarEstadoAprobacion,
    obtenerPorcentaje,
    obtenerResumenLey
} from  '@repository/programaciones';
import { mapToProgramaciones, mapToProgramacion, mapPorcentajes } from '@helpers/programaciones';

const registrar = async (req, res, next) => {
    const id = await upsert(req.body);
    if(id == null){
        res.status(200).json({
            OK:false, 
            error: "A ocurrido un error al momento de intentar registrar la programación"
        });
    }else{
        res.status(200).json({
            OK:true, 
            id: id
        });
    }
};

const actualizar = async (req, res, next) => {
    req.body.id = req.params.id;
    const result = await rActualizar(req.body);
    if(!result){
        res.status(200).json({
            OK:false, 
            error: "A ocurrido un error al momento de actualizar la programación"
        });
    }else{
        res.status(200).json({
            OK:  true
        });
    }
};

const cambiarEstadoAprobacion = async (req, res, next) => {
    const estado = req.body.estado;
    const result = await rCambiarEstadoAprobacion(req.params.id, estado);
    if(!result){
        res.status(200).json({
            OK:false, 
            error: "A ocurrido un error al momento de cambiar estado de aprobación de la programación"
        });
    }else{
        res.status(200).json({
            OK:  true
        });
    }
};

const eliminar = async (req, res, next) => {
    const result = await rEliminar(req.params.id);
    if(!result){
        res.status(200).json({
            OK:false, 
            error: "A ocurrido un error al momento de eliminar la programación"
        });
    }else{
        res.status(200).json({
            OK:  true
        });
    }
};


const obtenerPorContrato = async (req, res, next) => {
    const programaciones = mapToProgramaciones(await rObtenerPorContrato(req.params.id));

    res.status(200).json({
        OK:true, 
        programaciones: programaciones
    });
};

const obtenerPorId = async (req, res, next) => {
    const programacion = mapToProgramacion(await rObtenerPorId(req.params.id));
    if(!programacion){
        res.status(200).json({
            OK:false, 
            error: "No se ha encontrado la programación"
        });
    }else{
        res.status(200).json({
            OK:true, 
            programacion: programacion
        });
    }
};

const obtenerProgramacion = async (req, res, next) => {
    const porcenajeProgramacion = await obtenerPorcentaje(req.params.id);
    if(!porcenajeProgramacion){
        res.status(200).json({
            OK:false,
            error: "No se ha encontrado la programación"
        });
    }else{
        res.status(200).json({
            OK:true,
            programacion: mapPorcentajes(porcenajeProgramacion)
        });
    }
};

const obtenerResumenOriginal = async (req, res, next) => {
    const resumen = await obtenerResumenLey(req.body);
    if(!resumen){
        res.status(200).json({
            OK:false,
            error: "No se han encontrado datos"
        });
    }else{
        res.status(200).json({
            OK:true,
            data: resumen
        });
    }
};
const obtenerResumen = async (req,res,next) => {
    let resumen = [];
    resumen = await obtenerResumenLey(req.body);
    res.status(200).json({
        OK: true,
        data: resumen
    });
};

export {
    registrar,
    actualizar,
    eliminar,
    obtenerPorId,
    obtenerPorContrato,
    cambiarEstadoAprobacion,
    obtenerProgramacion,
    obtenerResumen,
};
