import { query } from './pg.connect';

const upsert = async (programacion) => {
    const q = `
        insert into "proplan"."programacion" (
            contrato_extension_id,actividad_id,periodo_id,especialidad_id,
            centro_costo_id,horas_asignadas,horas_rendimiento,horas_legales_descuento
        )
        values($1,$2,$3,$4,$5,$6,$7,$8)
        
        RETURNING id;
    `;
    /*ON CONFLICT (id) DO UPDATE SET
        horas_asignadas = $9,
        horas_rendimiento = $10,
        horas_legales_descuento = $11
        WHERE contrato_extension_id=$1 AND actividad_id=$2 AND periodo_id=$3 AND especialidad_id = $4 AND centro_costo_id = $5*/
    try {
        const result = await query(q,[
            programacion.extensionContrato,programacion.actividad,programacion.periodo,programacion.especialidad,
            programacion.centro_costo,programacion.horas.asignadas,programacion.horas.rendimiento,
            programacion.horas.legales_descuento

        ]);
        /* ,
            programacion.horas.asignadas,
            programacion.horas.rendimiento,
            programacion.horas.legales_descuento,  */
        return result.rows[0].id;
    } catch(e){
        console.log(e);
    }
    return null;
};

const actualizar = async (programacion) => {
    const q = `
        update "proplan"."programacion"
        set
            horas_asignadas = $1,
            horas_rendimiento = $2
        where id = $3;
    `;
    
    try{
        await query(q,[
            programacion.horas.asignadas,
            programacion.horas.rendimiento,
            programacion.id,
        ]);
        return true       
    }catch(e){
        console.log(e);
    }
    return false;
};

const cambiarEstadoAprobacion = async (id, aprobado) => {
    const q = `
        update "proplan"."programacion"
        set aprobado = $1
        where id = $2;
    `;
    
    try{
        await query(q,[aprobado ? 1 : 0, id]);
        return true       
    }catch(e){
        console.log(e);
    }
    return false;
};

const eliminar = async (id) => {
    const q = `
        delete from "proplan"."programacion"
        where id = $1;
    `;
    
    try{
        await query(q,[
            id,
        ]);
        return true       
    }catch(e){
        console.log(e);
    }
    return false;
};

const obtenerPorContrato = async (contratoExtensionId) => {
    let programaciones = [];
    const q = `
        select
            p.id, p.contrato_Extension_id,p.actividad_id,p.periodo_id,p.especialidad_id,
            p.centro_costo_id,p.horas_asignadas,p.horas_rendimiento,p.horas_legales_descuento,
            p.fecha_creacion_registro,p.aprobado,
            a.codigo actividad_codigo, a.descripcion actividad_descripcion,
            e.codigo especialidad_codigo, e.descripcion especialidad_descripcion,
            c.codigo centro_costo_codigo, c.descripcion centro_costo_descripcion
        from "proplan"."programacion" p
        inner join "proplan"."actividad" a on a.id = p.actividad_id
        left join "proplan"."especialidad" e on e.id = p.especialidad_id
        inner join "proplan"."centro_costo" c on c.id = p.centro_costo_id
        where p.contrato_Extension_id = $1;
    `;
    try{
        programaciones = await query(q,[contratoExtensionId]);      
    }catch(e){
        console.log(e);
    }
    return programaciones.rows;
};

const obtenerPorcentaje = async (idPeriodo) => {
    console.log(idPeriodo)
    let programaciones = [];
    const q = `
    SELECT pro.id, sum(COALESCE(p.horas_asignadas, 0)) as horas_programadas, ce.horas_semanales 
    FROM "proplan"."contrato_extension" ce
    left JOIN "proplan"."programacion" p ON (p.contrato_extension_id = ce.id)
    INNER JOIN "proplan"."contrato" c ON (c.id = ce.contrato_id)
    INNER JOIN "proplan"."profesional" pro ON (pro.id = c.profesional_id)
        ${idPeriodo != null ? 'INNER' : 'LEFT'} JOIN
            "proplan"."periodo" pe on pe.id = $1 and (c.fecha_termino is null or pe.fecha_inicio <= c.fecha_termino)
        WHERE ce.activo = true
        group by pro.id, ce.id
    `;
    try{
        programaciones = await query(q,[idPeriodo]);      
    }catch(e){
        console.log(e);
    }
    return programaciones.rows;
};

const obtenerPorId = async (id) => {
    let programacion = null;
    const q = `
        select
            p.id, p.contrato_extension_id,p.actividad_id,p.periodo_id,p.especialidad_id,
            p.centro_costo_id,p.horas_asignadas,p.horas_rendimiento,p.horas_legales_descuento,
            p.fecha_creacion_registro,p.aprobado,
            a.codigo actividad_codigo, a.descripcion actividad_descripcion,
            e.codigo especialidad_codigo, e.descripcion especialidad_descripcion,
            c.codigo centro_costo_codigo, c.descripcion centro_costo_descripcion
        from "proplan"."programacion" p
        inner join "proplan"."actividad" a on a.id = p.actividad_id
        left join "proplan"."especialidad" e on e.id = p.especialidad_id
        inner join "proplan"."centro_costo" c on c.id = p.centro_costo_id
        where p.id = $1;
    `;
    
    try{
        programacion = await query(q,[id]);
        if(programacion.rows.length > 0){
            programacion = programacion.rows[0];
        }else{
            programacion = null;
        }
    }catch(e){
        console.log(e);
    }
    return programacion;
};

const obtenerResumenLey = async (n) => {
    let resumen = null;
    const q = `
 select sum(horas_contratadas) as horas_contratadas, sum(horas_programadas) as horas_programadas from (
    select ce.horas_semanales as horas_contratadas, sum(p.horas_asignadas) as horas_programadas
    from "proplan"."contrato_extension" ce
    left  join "proplan"."programacion" p on ce.id = p.contrato_extension_id
    inner join  "proplan"."contrato" c on ce.contrato_id = c.id
    inner join  "proplan"."ley" l on c.ley_id = l.id
    inner join "proplan"."periodo" pe on pe.id = $1 and (c.fecha_termino is null or pe.fecha_inicio <= c.fecha_termino)
    where ce.activo = true and l.codigo = $2 and c.fecha_inicio between pe.fecha_inicio and pe.fecha_termino
    group by ce.id) t`;

    try{
        resumen = await query(q,[n.periodo,n.ley]);
        if(resumen.rows.length > 0){
            resumen = resumen.rows[0];
            if(!resumen.horas_contratadas){
                resumen.horas_contratadas = 0;
            }
            if(!resumen.horas_programadas){
                resumen.horas_programadas = 0;
            }
        }else{
            resumen = null;
        }
    }catch(e){
        console.log(e);
    }
    return resumen;
};

export {
    upsert,
    actualizar,
    eliminar,
    obtenerPorId,
    obtenerPorContrato,
    cambiarEstadoAprobacion,
    obtenerPorcentaje,
    obtenerResumenLey,
};
