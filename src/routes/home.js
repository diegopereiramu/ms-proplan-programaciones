import { 
    registrar, obtenerPorContrato,
    actualizar, eliminar, obtenerPorId,
    cambiarEstadoAprobacion,
    obtenerProgramacion, obtenerResumen

} from '@actions/home';

export default (router, app) => {
    router.post('/', registrar);
    router.put('/:id', actualizar);
    router.put('/:id/cambiar-estado-aprobacion', cambiarEstadoAprobacion);    
    router.delete('/:id', eliminar);
    router.get('/:id', obtenerPorId);
    router.get('/contrato/:id', obtenerPorContrato);
    router.get('/obtener-programaciones/:id', obtenerProgramacion);
    router.post('/obtener-resumen/', obtenerResumen);
};
